package com.tbs.supervisers.utils;

import android.app.Application;

public class CustomFont extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        TypefaceUtil.overrideFont(getApplicationContext(), "serif", "fonts/montserrat_light.ttf");
    }
}