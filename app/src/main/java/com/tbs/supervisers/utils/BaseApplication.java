package com.tbs.supervisers.utils;

import android.app.Application;

/*
 * Created by developer on 7/3/19.
 */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppPrefs.initPrefs(this);

    }
}
