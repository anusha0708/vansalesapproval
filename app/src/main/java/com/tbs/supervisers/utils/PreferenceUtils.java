package com.tbs.supervisers.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferenceUtils {

    private SharedPreferences preferences;
    private SharedPreferences.Editor edit;
    public static  String DAMAGE_REASON = "DAMAGE_REASON";
    public static  String DAMAGE_REASON_ID = "DAMAGE_REASON_ID";
    public static  String EMIRATES_ID = "EMIRATES_ID";
    public static  String COMMENT_ID = "COMMENT_ID";
    public static  String PRICE_TAG = "PRICE_TAG";
    public static  String CONFIG_SUCCESS = "CONFIG_SUCCESS";

    public static  String CAPTURE = "CAPTURE";
    public static  String DAMAGE = "DAMAGE";

    public static String STOCK_COUNT = "STOCK_COUNT";
    public static  String BANK_REASON = "BANK_REASON";
    public static String WEIGHT = "WEIGHT";
    public static String VOLUME = "VOLUME";
    public static String VEHICLE_CAPACITY = "VEHICLE_CAPACITY";
    public static String TOTAL_LOAD = "TOTAL_LOAD";
    public static String CUSTOMER = "CUSTOMER";
    public static String CUSTOMER_NAME = "CUSTOMER_NAME";
    public static String TOTAL_VEHICLE_CAPACITY = "TOTAL_VEHICLE_CAPACITY";
    public static String UNIT_CAPACITY = "UNIT_CAPACITY";
    public static String SITE_NAME = "SITE_NAME";
    public static String TOTAL_NON_SCHEDULED_STOCK = "TOTAL_NON_SCHEDULED_STOCK";

    public static String TOTAL_SCHEDULED_STOCK = "TOTAL_SCHEDULED_STOCK";
    public static String CREDIT_INVOICE_AMOUNT = "CREDIT_INVOICE_AMOUNT";

    public static String SELECTION_INVOICE_ID = "SELECTION_INVOICE_ID";
    public static String SHIPMENT_ID = "SHIPMENT_ID";
    public static String USER_NAME = "user_name";
    public static String PASSWORD = "password";

    public static String A_USER_NAME = "auth_user_name";
    public static String A_PASSWORD = "auth_password";
    public static String APP_VERSION = "APP_VERSION";

    public static String IP_ADDRESS = "address";
    public static String PORT = "port";
    public static String ALIAS = "alias";
    public static String VEHICLE_ROUTE_ID = "VEHICLE_ROUTE_ID";
    public static String Non_Scheduled_Route_Id = "NonScheduledRouteId";
    public static String PROFILE_PICTURE = "PROFILE_PICTURE";
    public static String VEHICLE_CHECKED_ROUTE_ID = "VEHICLE_CHECKED_ROUTE_ID";
    public static String SITE_ID = "SITE_ID";
    public static String SHIPMENT = "SHIPMENT";
    public static String USER_AUTHORIZED = "USER_AUTHORIZED";
    public static String ProductsType = "ProductsType";
    public static String CustomerId = "CustomerId";
    public static String ShipmentProductsType = "ShipmentProductsType";
    public static String PRODUCT_APPROVAL = "PRODUCT_APPROVAL";
    public static String RESCHEDULE_APPROVAL = "RESCHEDULE_APPROVAL";
    public static String DELIVERY_NOTE = "DELIVERY_NOTE";
    public static String DELIVERY_NOTE_RESHEDULE = "DELIVERY_NOTE_RESHEDULE";
    public static String PRODUCT_CODE = "PRODUCT_CODE";
    public static String PRODUCT_DESCRIPTION = "PRODUCT_DESCRIPTION";
    public static String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    public static String PAYMENT_TYPE = "PAYMENT_TYPE";
    public static String C_LAT = "C_LAT";
    public static String C_LONG = "C_LONG";

    public static String B_SITE_NAME = "B_SITE_NAME";
    public static String COMPANY = "COMPANY";
    public static String COMPANY_DES = "COMPANY_DES";
    public static String B_SITE_ID = "B_SITE_ID";
    public static String DATE_FORMAT = "DATE_FORMAT";
    public static String SPOT_DELIVERY_NUMBER = "SPOT_DELIVERY_NUMBER";
    public static String NONBG = "NONBG";

    public static String DELIVERY_DATE = "DELIVERY_DATE";

    public static String USER_ROLE = "USER_ROLE";

    public static String LOGIN_EMAIL = "LOGIN_EMAIL";

    public static String Reason_CODE = "Reason_CODE";

    public static String CARRIER_ID = "CARRIER_ID";
    public static String REASON = "REASON";
    public static String Reason_Payment = "Reason_Payment";

    public static String SELECTION = "SELECTION";
    public static String V_PLATE = "V_PLATE";
    public static String D_TIME = "D_TIME";
    public static String A_TIME = "A_TIME";
    public static String N_SHIPMENTS = "N_SHIPMENTS";
    public static String D_DATE = "D_DATE";
    public static String A_DATE = "A_DATE";
    public static String VEHICLE_CODE = "VEHICLE_CODE";
    public static String FIRST = "FIRST";
    public static String LOAN_RETURN_NUMBER = "LOAN_RETURN_NUMBER";

    public static String DOC_NUMBER = "DOC_NUMBER";

    public static String NON_VEHICLE_CODE = "NON_VEHICLE_CODE";
    public static String LOCATION = "LOCATION";

    public static String CV_PLATE = "CV_PLATE";
    public static String CD_TIME = "CD_TIME";
    public static String CA_TIME = "CA_TIME";
    public static String CN_SHIPMENTS = "CN_SHIPMENTS";
    public static String CD_DATE = "CD_DATE";
    public static String CA_DATE = "CA_DATE";
    public static String CVEHICLE_CODE = "CVEHICLE_CODE";

    public static String STATUS = "STATUS";


    public static String SHIPMENT_DT = "SHIPMENT_DT";
    public static String SHIPMENT_AT = "SHIPMENT_AT";
    public static String EM_CARRIER_ID = "EM_CARRIER_ID";
    public static String NOTES = "NOTES";
    public static String DRIVER_NAME = "DRIVER_NAME";
    public static String LOGIN_DRIVER_NAME = "LOGIN_DRIVER_NAME";
    public static String DISTANCE = "EMAIL";

    public static String EMAIL = "EMAIL";
    public static String PHONE = "PHONE";
    public static String LONGITUDE2 = "LONG2";
    public static String LATTITUDE2 = "LAT2";
    public static String LONGITUDE = "LONG";
    public static String LATTITUDE = "LAT";
    public static String CHECKIN_DATE = "CHECKIN_DATE";
    public static String DRIVER_ID = "DRIVER_ID";
    public static String VR_ID = "VR_ID";
    public static String CHEQUE_DATE = "CHEQUE_DATE";

    public static String INVOICE_ID = "INVOICE_ID";
    public static String PAYMENT_ID = "PAYMENT_ID";
    public static String BASE_VEHICLE_ROUTE_ID = "BASE_VEHICLE_ROUTE_ID";
    public static String INVOICE_AMOUNT = "INVOICE_AMOUNT";
    public static String CURRENCY = "CURRENCY";
    public static String INVOICE_SHIPMENT_ID = "INVOICE_SHIPMENT_ID";

    public static String PREVIEW_INVOICE_ID = "PREVIEW_INVOICE_ID";

    public static String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    public static String PAYMENT_INVOICE_ID  = "PAYMENT_INVOICE_ID";

    public static String CHECK_IN_ARRIVAL = "CHECK_IN_ARRIVAL";
    public static String CHECK_IN_START_LOADING = "CHECK_IN_START_LOADING";
    public static String CHECK_IN_END_LOADING = "CHECK_IN_END_LOADING";
    public static String CHECK_IN_STATUS = "CHECK_IN_STATUS";
    public static String CHECK_IN_SHEDULED = "CHECK_IN_SHEDULED";
    public static String CHECK_IN_NONSHEDULED = "CHECK_IN_NONSHEDULED";
    public static String CHECK_IN_VEHICLE_INSPECTION = "CHECK_IN_VEHICLE_INSPECTION";
    public static String CHECK_IN_GATE_INSPECTION = "CHECK_IN_GATE_INSPECTION";
    public static String CHECK_IN_ARRIVAL_TIME = "CHECK_IN_ARRIVAL_TIME";
    public static String CHECK_IN_START_LOADING_TIME = "CHECK_IN_START_LOADING_TIME";
    public static String CHECK_IN_END_LOADING_TIME = "CHECK_IN_END_LOADING_TIME";
    public static String CHECK_IN_STATUS_TIME = "CHECK_IN_STATUS_TIME";
    public static String CHECK_IN_LOADED_STOCK = "CHECK_IN_LOADED_STOCK";

    public static String CHECK_IN_VEHICLE_INSPECTION_TIME = "CHECK_IN_VEHICLE_INSPECTION_TIME ";
    public static String CHECK_IN_GATE_INSPECTION_TIME = "CHECK_IN_GATE_INSPECTION_TIME ";



    public static  String GRP = "GRP";

    public PreferenceUtils(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        edit = preferences.edit();
    }

    public void clearPreferences() {
        edit.clear().apply();
        edit.commit();
    }

    public void saveString(String strKey, String strValue) {
        edit.putString(strKey, strValue);
        edit.commit();
    }

    public void saveInt(String strKey, int value) {
        edit.putInt(strKey, value);
        edit.commit();
    }


    public void saveLong(String strKey, Long value) {
        edit.putLong(strKey, value);
        edit.commit();
    }

    public void saveFloat(String strKey, float value) {
        edit.putFloat(strKey, value);
        edit.commit();
    }

    public void saveDouble(String strKey, Double value) {
        edit.putString(strKey, "" + value);
        edit.commit();
    }

    public void saveBoolean(String strKey, boolean value) {
        edit.putBoolean(strKey, value);
        edit.commit();
    }

    public void removeFromPreference(String strKey) {
        edit.remove(strKey);

        edit.apply();
        edit.commit();
    }

    public String getStringFromPreference(String strKey, String defaultValue) {
        return preferences.getString(strKey, defaultValue);
    }

    public boolean getbooleanFromPreference(String strKey, boolean defaultValue) {
        return preferences.getBoolean(strKey, defaultValue);
    }

    public int getIntFromPreference(String strKey, int defaultValue) {

        return preferences.getInt(strKey, defaultValue);
    }

    public long getLongFromPreference(String strKey) {
        return preferences.getLong(strKey, 0);
    }

    public float getFloatFromPreference(String strKey, float defaultValue) {
        return preferences.getFloat(strKey, defaultValue);
    }

    public double getDoubleFromPreference(String strKey, double defaultValue) {
        return Double.parseDouble(preferences.getString(strKey, "" + defaultValue));
    }
}
