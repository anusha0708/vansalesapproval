package com.tbs.supervisers.utils;

public class WebServiceConstants {
    public static final String CONFIGURATION         = "XX10CCONF";
    public static final String LOGIN                 = "YAPRLOGIN";
    public static final String MULTIPLE_LOGIN        = "XLMLGNIN";
    public static final String LOGIN_TIME_CAPTURE    = "X10CUSRLGN";
    public static final String REQUEST_LIST          = "YREQLIST";
    public static final String ACCEPT_REJECT         = "YREQAPRVL";
    public static final String LOGOUT_TIMECAPTURE    = "YAPRLOGOUT";
    public static final String ACTIVITY_REPORT       = "YAPRVLRPT";

}
