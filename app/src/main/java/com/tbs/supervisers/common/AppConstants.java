package com.tbs.supervisers.common;



import java.io.File;
import java.util.ArrayList;

public class AppConstants {



    public static String DATABASE_NAME = "brothersgas.sqlite";
    public static String DATABASE_PATH = "";

    public static final String Goudy_Old_Style_Bold			= "bgb.ttf";
    public static final String Goudy_Old_Style_Italic			    = "bgi.ttf";
    public static final String Goudy_Old_Style_Normal			    = "bgs.ttf";

    public static final String MONTSERRAT_REGULAR_TYPE_FACE			= "montserrat_regular.ttf";
    public static final String MONTSERRAT_MEDIUM_TYPE_FACE			= "montserrat_medium.ttf";
    public static final String MONTSERRAT_BOLD_TYPE_FACE			= "montserrat_bold.ttf";
    public static final String MONTSERRAT_LIGHT_TYPE_FACE			= "montserrat_light.ttf";
    public static String message="";

}
