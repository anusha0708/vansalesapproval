package com.tbs.supervisers.adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.tbs.supervisers.R;
import com.tbs.supervisers.activitys.BaseActivity;
import com.tbs.supervisers.activitys.RequestsListActivity;
import com.tbs.supervisers.model.OrderRequestsDO;
import com.tbs.supervisers.model.SuccessDO;
import com.tbs.supervisers.utils.PreferenceUtils;
import com.tbs.supervisers.webaccess.AcceptRejectRequest;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.MyViewHolder> {
    int count = 0;
    private LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap;
    private ArrayList<OrderRequestsDO> availableDOS;
    private Context context;
    private int type;
    private PreferenceUtils preferenceUtils;

    public void refreshAdapter(Context context, @NotNull LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap, ArrayList<OrderRequestsDO> availableDos) {
        this.orderRequestsDosMap.clear();
        this.availableDOS.clear();
        this.orderRequestsDosMap = orderRequestsDosMap;
        this.availableDOS.addAll(availableDos);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductDescription, tvActualQuantity, tvUpdatedQuantity, tvQuantityChange, tvOrderID, tvRegisteredDate, tvRegisteredUser, tvLevelOfSignature, tvAccept, tvReject;
        private RecyclerView rvReturnProducts;
        private ImageView ivPhone;

        public MyViewHolder(View view) {
            super(view);
            tvProductDescription = view.findViewById(R.id.tvProductDescription);
            tvActualQuantity = view.findViewById(R.id.tvActualQuantity);
            tvUpdatedQuantity = view.findViewById(R.id.tvUpdatedQuantity);
            tvQuantityChange = view.findViewById(R.id.tvQuantityChange);
            tvOrderID = view.findViewById(R.id.tvOrderID);
            tvRegisteredDate = view.findViewById(R.id.tvRegisteredDate);
            ivPhone = view.findViewById(R.id.ivPhone);
            tvAccept = view.findViewById(R.id.tvAccept);
            tvReject = view.findViewById(R.id.tvReject);
            tvRegisteredUser = view.findViewById(R.id.tvRegisteredUser);
            tvLevelOfSignature = view.findViewById(R.id.tvLevelOfSignature);
            rvReturnProducts = view.findViewById(R.id.rvList);

        }
    }


    public RequestsAdapter(Context context, LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap, ArrayList<OrderRequestsDO> availableDos) {
        this.context = context;
        this.orderRequestsDosMap = orderRequestsDosMap;
        this.availableDOS = availableDos;
        preferenceUtils = new PreferenceUtils(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_cell, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ArrayList<String> keySet = new ArrayList<>(orderRequestsDosMap.keySet());

        ArrayList<OrderRequestsDO> orderRequestsDos = orderRequestsDosMap.get(keySet.get(position));
        if (availableDOS != null && availableDOS.size() > 0) {
            final OrderRequestsDO orderRequestsDO = availableDOS.get(position);
            String dMonth = orderRequestsDO.date.substring(4, 6);
            String dyear = orderRequestsDO.date.substring(0, 4);
            String dDate = orderRequestsDO.date.substring(Math.max(orderRequestsDO.date.length() - 2, 0));
            if (orderRequestsDO.documentType == 1) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Actual Qty");
                holder.tvUpdatedQuantity.setText("Updated Qty");
            } else if (orderRequestsDO.documentType == 2) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Actual Price");
                holder.tvUpdatedQuantity.setText("Updated Price");
            } else if (orderRequestsDO.documentType == 3) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.GONE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Quantity");
                holder.tvUpdatedQuantity.setText("");
            } else if (orderRequestsDO.documentType == 4) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.GONE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Quantity");
//                holder.tvUpdatedQuantity.setText("Reason");
            } else if (orderRequestsDO.documentType == 5) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);

                holder.tvProductDescription.setText("Delivery Number");
                holder.tvActualQuantity.setText("Sequence");
                holder.tvUpdatedQuantity.setText("Reason");
            } else if (orderRequestsDO.documentType == 6) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
                holder.tvProductDescription.setText("Delivery Number");
                holder.tvActualQuantity.setText("Date");
                holder.tvUpdatedQuantity.setText("Reason");
            } else if (orderRequestsDO.documentType == 7) {
                holder.tvActualQuantity.setVisibility(View.GONE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvProductDescription.setText("Delivery Number");
                holder.tvActualQuantity.setText("");
                holder.tvUpdatedQuantity.setText("Reason");
            } else if (orderRequestsDO.documentType == 8) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Quantity");
                holder.tvUpdatedQuantity.setText("Reason");
            }
            if (orderRequestsDO.documentType == 1) {
                holder.tvRegisteredDate.setText(orderRequestsDO.customerName + "\n" +
                        orderRequestsDO.address + "\n" + orderRequestsDO.cityEmirates + "\n" + orderRequestsDO.deliveryNumber + "\n" + dDate + "-" + dMonth + "-" + dyear + "   " + orderRequestsDO.time);
//            holder.tvRegisteredDate.setText(orderRequestsDO.deliveryNumber + "\n" + dDate + "-" + dMonth + "-" + dyear + "   " + orderRequestsDO.time);

            } else {
                holder.tvRegisteredDate.setText(orderRequestsDO.customerName + "\n" +
                        orderRequestsDO.address + "\n" + orderRequestsDO.cityEmirates + "\n" + "" + dDate + "-" + dMonth + "-" + dyear + "   " + orderRequestsDO.time);
//            holder.tvRegisteredDate.setText("" + dDate + "-" + dMonth + "-" + dyear + "   " + orderRequestsDO.time);

            }
            holder.tvQuantityChange.setText("" + orderRequestsDO.documentDescription);
            holder.tvOrderID.setText("" + orderRequestsDO.requestID);
            holder.tvRegisteredUser.setText("" + orderRequestsDO.userID + " - " + orderRequestsDO.userName);
            if (!orderRequestsDO.signedUser1.isEmpty() && !orderRequestsDO.signedUser2.isEmpty()) {
                holder.tvLevelOfSignature.setText("LOS - " + orderRequestsDO.signatureLevel
                        + " \n" + orderRequestsDO.signedUser1 + "\n" + orderRequestsDO.signedUser2);

            } else if (!orderRequestsDO.signedUser1.isEmpty()) {
                holder.tvLevelOfSignature.setText("LOS - " + orderRequestsDO.signatureLevel
                        + " \n" + orderRequestsDO.signedUser1);

            } else if (!orderRequestsDO.signedUser2.isEmpty()) {
                holder.tvLevelOfSignature.setText("LOS - " + orderRequestsDO.signatureLevel
                        + "\n" + orderRequestsDO.signedUser2);

            } else {

                holder.tvLevelOfSignature.setText("LOS - " + orderRequestsDO.signatureLevel);
            }

            holder.rvReturnProducts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            holder.rvReturnProducts.setAdapter(new ReturnProductsListAdapter(context, orderRequestsDos));
            holder.ivPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ((BaseActivity) context).hideLoader();

                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + orderRequestsDO.phoneNumber));
                        context.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            holder.tvAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ((BaseActivity) context).hideLoader();

                        AcceptRejectRequest siteListRequest = new AcceptRejectRequest(orderRequestsDO.requestID, 2, context);
                        ((BaseActivity) context).showLoader();
                        siteListRequest.setOnResultListener(new AcceptRejectRequest.OnResultListener() {
                            @Override
                            public void onCompleted(boolean isError, SuccessDO successDO) {
                                ((BaseActivity) context).hideLoader();

                                if (isError) {
                                    Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                                } else {
                                    if (successDO.flag == 20) {
                                        ((BaseActivity) context).showToast("Accepted successfully");
                                    }
                                    ((RequestsListActivity) context).getData();

                                }


                            }
                        });

                        siteListRequest.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            holder.tvReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ((BaseActivity) context).hideLoader();

                        AcceptRejectRequest siteListRequest = new AcceptRejectRequest(orderRequestsDO.requestID, 3, context);
                        ((BaseActivity) context).showLoader();
                        siteListRequest.setOnResultListener(new AcceptRejectRequest.OnResultListener() {
                            @Override
                            public void onCompleted(boolean isError, SuccessDO successDO) {
                                ((BaseActivity) context).hideLoader();

                                if (isError) {
                                    Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                                } else {
                                    if (successDO.flag == 30) {
                                        ((BaseActivity) context).showToast("Rejected successfully");

                                    }
                                    ((RequestsListActivity) context).getData();

                                }


                            }
                        });

                        siteListRequest.execute();
                    } catch (Exception e) {

                    }

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return orderRequestsDosMap != null ? orderRequestsDosMap.keySet().size() : 0;
    }

    private class ReturnProductsListAdapter extends RecyclerView.Adapter<ReturnListHolder> {
        private ArrayList<OrderRequestsDO> orderRequestsDos;
        private Context context;

        ReturnProductsListAdapter(Context context, ArrayList<OrderRequestsDO> orderRequestsDos) {
            this.context = context;
            this.orderRequestsDos = orderRequestsDos;
        }

        @NonNull
        @Override
        public ReturnListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.order_item_cell, viewGroup, false);
            return new ReturnListHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ReturnListHolder holder, int position) {
            final OrderRequestsDO orderRequestsDO = orderRequestsDos.get(position);
//            holder.tvProductName.setText(orderRequestsDO.product);
            Double actualQty = (Double) orderRequestsDO.actualQuantity;
            Double updatedQty = (Double) orderRequestsDO.updatedQuantity;
            Double actualPrice = (Double) orderRequestsDO.actualPrice;
            Double updatedPrice = (Double) orderRequestsDO.updatedPrice;

            if (orderRequestsDO.documentType == 1) {
                holder.tvProductDescription.setText(orderRequestsDO.productDescription);
                holder.tvActualQuantity.setText("" + actualQty + " " + orderRequestsDO.unit);
                holder.tvUpdatedQuantity.setText("" + updatedQty + " " + orderRequestsDO.unit);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 2) {
                holder.tvProductDescription.setText(orderRequestsDO.productDescription);
                holder.tvActualQuantity.setText("" + actualPrice + " AED");
                holder.tvUpdatedQuantity.setText("" + updatedPrice + " AED");
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 3) {
                holder.tvProductDescription.setText(orderRequestsDO.productDescription);
                holder.tvActualQuantity.setVisibility(View.GONE);
                holder.tvUpdatedQuantity.setText("" + updatedQty + " " + orderRequestsDO.unit);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 4) {
                holder.tvProductDescription.setText(orderRequestsDO.productDescription + "\n" + orderRequestsDO.loanDeliveryNumber);
                holder.tvActualQuantity.setText("" + updatedQty + " " + orderRequestsDO.unit);
                holder.tvUpdatedQuantity.setText("" + orderRequestsDO.reasons);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.GONE);
            } else if (orderRequestsDO.documentType == 5 && position == 0) {

                holder.tvProductDescription.setText(orderRequestsDO.deliveryNumber);
                holder.tvActualQuantity.setText("" + orderRequestsDO.sequenceID);
                holder.tvUpdatedQuantity.setText("" + orderRequestsDO.reasons);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 6 && position == 0) {
                if (orderRequestsDO.rescheduleDate.length() > 0) {

                    String dMonth = orderRequestsDO.rescheduleDate.substring(4, 6);
                    String dyear = orderRequestsDO.rescheduleDate.substring(0, 4);
                    String dDate = orderRequestsDO.rescheduleDate.substring(Math.max(orderRequestsDO.rescheduleDate.length() - 2, 0));
                    holder.tvActualQuantity.setText("" + dDate + "-" + dMonth + "-" + dyear);

                }
                holder.tvProductDescription.setText(orderRequestsDO.deliveryNumber);
                holder.tvUpdatedQuantity.setText("" + orderRequestsDO.reasons);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 7 && position == 0) {
                holder.tvProductDescription.setText(orderRequestsDO.deliveryNumber);
                holder.tvActualQuantity.setVisibility(View.GONE);
                holder.tvUpdatedQuantity.setText("" + orderRequestsDO.reasons);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 8) {
                if (!orderRequestsDO.reasons.isEmpty()) {
                    holder.tvProductDescription.setVisibility(View.VISIBLE);
                    holder.tvProductDescription.setText(orderRequestsDO.productDescription);
                    holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
                    holder.tvActualQuantity.setText("" + updatedQty);

                    holder.tvActualQuantity.setVisibility(View.VISIBLE);
                    holder.tvUpdatedQuantity.setText("" + orderRequestsDO.reasons);
                } else {
                    holder.tvProductDescription.setVisibility(View.GONE);
                    holder.tvUpdatedQuantity.setVisibility(View.GONE);
                    holder.tvActualQuantity.setVisibility(View.GONE);
                }

            } else {

                holder.tvProductDescription.setVisibility(View.GONE);
                holder.tvUpdatedQuantity.setVisibility(View.GONE);
                holder.tvActualQuantity.setVisibility(View.GONE);


            }


//            holder.tv.setText("" + actualPrice);
//            holder.tvUpdatedQuantity.setText("" + updatedPrice);


        }

        @Override
        public int getItemCount() {
            return orderRequestsDos != null ? orderRequestsDos.size() : 0;
        }
    }

    private class ReturnListHolder extends RecyclerView.ViewHolder {
        private TextView tvActualQuantity, tvProductName, tvProductDescription, tvUpdatedQuantity, tvNumber;
        private CheckBox cbSelected, cbNonBGSelected;
        private EditText etNumberET;
        private ImageView ivAdd, ivRemove;

        public ReturnListHolder(@NonNull View itemView) {
            super(itemView);
//            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);

            tvActualQuantity = itemView.findViewById(R.id.tvActualQuantity);
            tvUpdatedQuantity = itemView.findViewById(R.id.tvUpdatedQuantity);


        }
    }
}
