package com.tbs.supervisers.adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tbs.supervisers.R;
import com.tbs.supervisers.activitys.BaseActivity;
import com.tbs.supervisers.activitys.RequestsListActivity;
import com.tbs.supervisers.model.OrderRequestsDO;
import com.tbs.supervisers.model.SuccessDO;
import com.tbs.supervisers.utils.PreferenceUtils;
import com.tbs.supervisers.webaccess.AcceptRejectRequest;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.MyViewHolder> {
    private ArrayList<OrderRequestsDO> orderRequestsDOS;
    Context context;
    private LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType, tvStatus,tvDocStatus, tvCustomer, tvDateTime, tvDocument, tvRequest,tvProductDescription, tvActualQuantity, tvUpdatedQuantity;
        private RecyclerView rvReturnProducts;
        private View view1,view2;
        private LinearLayout llItem;

        public MyViewHolder(View view) {
            super(view);
            tvRequest = view.findViewById(R.id.tvRequest);
            tvDocument = view.findViewById(R.id.tvDocument);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvCustomer = view.findViewById(R.id.tvCustomer);
            tvType = view.findViewById(R.id.tvType);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvDocStatus = view.findViewById(R.id.tvDocStatus);
            rvReturnProducts = view.findViewById(R.id.rvList);
            tvProductDescription = view.findViewById(R.id.tvProductDescription);
            tvActualQuantity = view.findViewById(R.id.tvActualQuantity);
            tvUpdatedQuantity = view.findViewById(R.id.tvUpdatedQuantity);
            view1 = view.findViewById(R.id.view1);
            view2 = view.findViewById(R.id.view2);
            llItem = view.findViewById(R.id.llItem);

        }
    }


    public ReportAdapter(Context context,@NotNull LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap1,  ArrayList<OrderRequestsDO> orderRequestsDOS1) {
        this.context = context;
        this.orderRequestsDOS = orderRequestsDOS1;
        this.orderRequestsDosMap=orderRequestsDosMap1;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_cell, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ArrayList<String> keySet = new ArrayList<>(orderRequestsDosMap.keySet());

        ArrayList<OrderRequestsDO> orderRequestsDos = orderRequestsDosMap.get(keySet.get(position));
        if (orderRequestsDOS != null && orderRequestsDOS.size() > 0) {
            final OrderRequestsDO orderRequestsDO = orderRequestsDOS.get(position);
            String dMonth = orderRequestsDO.requestedDate.substring(4, 6);
            String dyear  = orderRequestsDO.requestedDate.substring(0, 4);
            String dDate  = orderRequestsDO.requestedDate.substring(Math.max(orderRequestsDO.requestedDate.length() - 2, 0));
            holder.tvRequest.setText("Request : " + orderRequestsDO.requestID);
            holder.tvDocument.setText("Document : " + orderRequestsDO.deliveryNumber);
            holder.tvDateTime.setText("Date & Time : " + dDate + "-" + dMonth + "-" + dyear + " " + orderRequestsDO.requestedTime);
            holder.tvCustomer.setText("" +orderRequestsDO.customer+" - "+ orderRequestsDO.customerName);
            holder.tvType.setText("Type : " + orderRequestsDO.requestedType);
            holder.tvStatus.setText("Status : " + orderRequestsDO.status + "(LOS-" + orderRequestsDO.signatureLevel + ")");
            holder.tvDocStatus.setText("Final Status : " + orderRequestsDO.docStatus);
            holder.rvReturnProducts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            holder.rvReturnProducts.setAdapter(new ReturnProductsListAdapter(context, orderRequestsDos));
            if (orderRequestsDO.documentType == 1) {
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
                holder.rvReturnProducts.setVisibility(View.VISIBLE);
                holder.view1.setVisibility(View.VISIBLE);
                holder.view2.setVisibility(View.VISIBLE);
                holder.llItem.setVisibility(View.VISIBLE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Actual Qty");
                holder.tvUpdatedQuantity.setText("Updated Qty");
            } else if (orderRequestsDO.documentType == 2) {
                holder.view1.setVisibility(View.VISIBLE);
                holder.view2.setVisibility(View.VISIBLE);
                holder.llItem.setVisibility(View.VISIBLE);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
                holder.rvReturnProducts.setVisibility(View.VISIBLE);

                holder.tvProductDescription.setText("Product Name");
                holder.tvActualQuantity.setText("Actual Price");
                holder.tvUpdatedQuantity.setText("Updated Price");
            } else {
                holder.tvProductDescription.setVisibility(View.GONE);
                holder.tvActualQuantity.setVisibility(View.GONE);
                holder.tvUpdatedQuantity.setVisibility(View.GONE);
                holder.rvReturnProducts.setVisibility(View.GONE);
                holder.view1.setVisibility(View.GONE);
                holder.view2.setVisibility(View.GONE);
                holder.llItem.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return orderRequestsDOS != null ? orderRequestsDOS.size() : 0;
    }


    private class ReturnProductsListAdapter extends RecyclerView.Adapter<ReturnListHolder> {
        private ArrayList<OrderRequestsDO> orderRequestsDos;
        private Context context;

        ReturnProductsListAdapter(Context context, ArrayList<OrderRequestsDO> orderRequestsDos) {
            this.context = context;
            this.orderRequestsDos = orderRequestsDos;
        }

        @NonNull
        @Override
        public ReturnListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.order_item_cell, viewGroup, false);
            return new ReturnListHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ReturnListHolder holder, int position) {
            final OrderRequestsDO orderRequestsDO = orderRequestsDos.get(position);

//            holder.tvProductName.setText(orderRequestsDO.product);
            Double actualQty = (Double) orderRequestsDO.actualQuantity;
            Double updatedQty = (Double) orderRequestsDO.updatedQuantity;
            Double actualPrice = (Double) orderRequestsDO.actualPrice;
            Double updatedPrice = (Double) orderRequestsDO.updatedPrice;

            if (orderRequestsDO.documentType == 1) {
                holder.tvProductDescription.setText(orderRequestsDO.productDescription);
                holder.tvActualQuantity.setText("" + actualQty + " " + orderRequestsDO.unit);
                holder.tvUpdatedQuantity.setText("" + updatedQty + " " + orderRequestsDO.unit);
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else if (orderRequestsDO.documentType == 2) {
                holder.tvProductDescription.setText(orderRequestsDO.productDescription);
                holder.tvActualQuantity.setText("" + actualPrice + " AED");
                holder.tvUpdatedQuantity.setText("" + updatedPrice + " AED");
                holder.tvProductDescription.setVisibility(View.VISIBLE);
                holder.tvActualQuantity.setVisibility(View.VISIBLE);
                holder.tvUpdatedQuantity.setVisibility(View.VISIBLE);
            } else {

                holder.tvProductDescription.setVisibility(View.GONE);
                holder.tvUpdatedQuantity.setVisibility(View.GONE);
                holder.tvActualQuantity.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return orderRequestsDos != null ? orderRequestsDos.size() : 0;
        }
    }

    private class ReturnListHolder extends RecyclerView.ViewHolder {
        private TextView tvActualQuantity, tvProductName, tvProductDescription, tvUpdatedQuantity, tvNumber;
        private CheckBox cbSelected, cbNonBGSelected;
        private EditText etNumberET;
        private ImageView ivAdd, ivRemove;

        public ReturnListHolder(@NonNull View itemView) {
            super(itemView);
//            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);

            tvActualQuantity = itemView.findViewById(R.id.tvActualQuantity);
            tvUpdatedQuantity = itemView.findViewById(R.id.tvUpdatedQuantity);


        }
    }
}
