//package com.tbs.supervisers.adapters;
//
///**
// * Created by sandy on 2/7/2018.
// */
//
//import android.content.Context;
//import android.content.Intent;
//import android.net.Uri;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//
//import com.tbs.supervisers.R;
//import com.tbs.supervisers.activitys.BaseActivity;
//import com.tbs.supervisers.model.OrderRequestsDO;
//import com.tbs.supervisers.utils.PreferenceUtils;
//
//import java.util.ArrayList;
//
//public class OrderRequestsAdapter extends RecyclerView.Adapter<OrderRequestsAdapter.MyViewHolder> {
//
//    private ArrayList<OrderRequestsDO> orderRequestsDOS;
//    private Context context;
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        private TextView tvQuantityChange,tvRegisteredDate,tvEventDate,tvReject,tvAccept;
//        private ImageView ivCall;
//
//        public MyViewHolder(View view) {
//            super(view);
//            tvQuantityChange             = (TextView)  itemView.findViewById(R.id.tvQuantityChange);
//            tvRegisteredDate   = (TextView)  itemView.findViewById(R.id.tvRegisteredDate);
//            tvReject           = (TextView)  itemView.findViewById(R.id.tvReject);
//            tvAccept           = (TextView)  itemView.findViewById(R.id.tvAccept);
//            ivCall             = (ImageView) itemView.findViewById(R.id.ivCall);
//
//        }
//    }
//
//
//    public OrderRequestsAdapter(Context context, ArrayList<OrderRequestsDO> siteDOS) {
//        this.context = context;
//        this.orderRequestsDOS = siteDOS;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.order_request_cell, parent, false);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//
//        final OrderRequestsDO orderRequestsDO = orderRequestsDOS.get(position);
////        final OrderRequestsDO orderRequestsDO = new OrderRequestsDO();
//
//        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
////        holder.tvName.setText(orderRequestsDO.name);
////        holder.tvEventDate.setText(orderRequestsDO.eventDate);
////        holder.tvRegisteredDate.setText("Registered On  : "+orderRequestsDO.registeredDate);
//        holder.tvAccept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//        holder.tvReject.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((BaseActivity)context).showAppCompatAlert("Alert!", "Are you sure you want to reject?", "Ok", "Cancel", "OrderReject", false);
//
//            }
//        });
//        holder.tvAccept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                orderId = orderRequestsDO.requestid;
//                ((BaseActivity)context).showAppCompatAlert("Alert!", "Are you sure you want to accept?", "Ok", "Cancel", "OrderAccept", false);
//            }
//        });
//
//        holder.ivCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    Intent intent = new Intent(Intent.ACTION_DIAL);
//                    intent.setData(Uri.parse("tel:"+orderRequestsDO.phoneNumber));
//                   context. startActivity(intent);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//
//
//    }
//    public void removeItem(int position) {
//        orderRequestsDOS.remove(position);
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, orderRequestsDOS.size());
//    }
//    public void restoreItem(OrderRequestsDO model, int position) {
//        orderRequestsDOS.add(position, model);
//        // notify item added by position
//        notifyItemInserted(position);
//    }
//    public void refreshAdapter(ArrayList<OrderRequestsDO> siteDoS) {
//        for (int i = 0; i < siteDoS.size(); i++) {
//
//        }
//        orderRequestsDOS = siteDoS;
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public int getItemCount() {
//        return orderRequestsDOS.size();
//    }
//
//}
