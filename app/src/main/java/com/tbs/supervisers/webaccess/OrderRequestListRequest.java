package com.tbs.supervisers.webaccess;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.supervisers.R;
import com.tbs.supervisers.activitys.BaseActivity;
import com.tbs.supervisers.common.AppConstants;
import com.tbs.supervisers.database.StorageManager;
import com.tbs.supervisers.model.OrderRequestsDO;
import com.tbs.supervisers.model.OrderRequestsMainDO;
import com.tbs.supervisers.utils.PreferenceUtils;
import com.tbs.supervisers.utils.ProgressTask;
import com.tbs.supervisers.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class OrderRequestListRequest extends AsyncTask<String, Void, Boolean> {
    String message = "";

    private OrderRequestsMainDO orderRequestsMainDO;
    private OrderRequestsDO orderRequestsDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public OrderRequestListRequest(Context mContext) {

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, OrderRequestsMainDO orderRequestsMainDO, String message);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CustomerId, "");

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.REQUEST_LIST);
        String driverID = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YUSER", driverID);


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                String error= response.getProperty(0).toString();
                String [] messages=error.split( "message=");
                error=messages[1].replace("]","");
                error=error.replace("}","");
                error=error.replace(";","");
                error=error.replace(".","");
                message = error;

                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            orderRequestsMainDO = new OrderRequestsMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        orderRequestsMainDO.orderRequestsDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        orderRequestsDO = new OrderRequestsDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {
                                orderRequestsMainDO.flag = Integer.parseInt(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_YREQNO")) {
                            if (text.length() > 0) {
                                orderRequestsDO.requestID = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YREQUSERID")) {
                            if (text.length() > 0) {
                                orderRequestsDO.userID = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YREQUSERNAME")) {
                            if (text.length() > 0) {
                                orderRequestsDO.userName = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YREQUSERPHNO")) {
                            if (text.length() > 0) {
                                orderRequestsDO.phoneNumber = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSAU")) {
                            if (text.length() > 0) {
                                orderRequestsDO.unit = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCNUM")) {
                            if (text.length() > 0) {
                                orderRequestsDO.deliveryNumber = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YREQDATE")) {
                            if (text.length() > 0) {
                                orderRequestsDO.date = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YBPNAM")) {
                            if (text.length() > 0) {
                                orderRequestsDO.customerName = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YBPADD")) {
                            if (text.length() > 0) {
                                orderRequestsDO.address = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_CTYEMI")) {
                            if (text.length() > 0) {
                                orderRequestsDO.cityEmirates = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_SUSR1")) {
                            if (text.length() > 0) {
                                orderRequestsDO.signedUser1 = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_SUSR2")) {
                            if (text.length() > 0) {
                                orderRequestsDO.signedUser2 = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSIGNLEVEL")) {
                            if (text.length() > 0) {
                                orderRequestsDO.signatureLevel = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCTYP")) {
                            if (text.length() > 0) {
                                orderRequestsDO.documentType = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSEQ")) {
                            if (text.length() > 0) {
                                orderRequestsDO.sequenceID = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCNUM1")) {
                            if (text.length() > 0) {
                                orderRequestsDO.loanDeliveryNumber = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YRESDATE")) {
                            if (text.length() > 0) {
                                orderRequestsDO.rescheduleDate = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCLINE")) {
                            if (text.length() > 0) {
                                orderRequestsDO.lineNumber = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCDES")) {
                            if (text.length() > 0) {
                                orderRequestsDO.documentDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITM")) {
                            if (text.length() > 0) {
                                orderRequestsDO.product = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                            if (text.length() > 0) {
                                orderRequestsDO.productDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YACTQTY")) {
                            if (text.length() > 0) {
                                orderRequestsDO.actualQuantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YUPDQTY")) {
                            if (text.length() > 0) {
                                orderRequestsDO.updatedQuantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YACTPRI")) {
                            if (text.length() > 0) {
                                orderRequestsDO.actualPrice = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YUPDPRI")) {
                            if (text.length() > 0) {
                                orderRequestsDO.updatedPrice = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YREASONS")) {
                            if (text.length() > 0) {
                                orderRequestsDO.reasons =text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCOMMENTS")) {
                            if (text.length() > 0) {
                                orderRequestsDO.comments =text;
                            }

                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        orderRequestsMainDO.orderRequestsDOS.add(orderRequestsDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        }  catch (SocketTimeoutException e) {
            e.printStackTrace();
            message = "Socket Time Out Exception Please try again";
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            message = " " + e;

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

//        ((BaseActivity)mContext).showLoader();
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
//        ((BaseActivity)mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, orderRequestsMainDO,message);
        }
    }
}