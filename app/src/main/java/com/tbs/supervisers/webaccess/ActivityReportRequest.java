package com.tbs.supervisers.webaccess;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.supervisers.model.OrderRequestsDO;
import com.tbs.supervisers.model.OrderRequestsMainDO;
import com.tbs.supervisers.utils.PreferenceUtils;
import com.tbs.supervisers.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ActivityReportRequest extends AsyncTask<String, Void, Boolean> {
    String message = "";

    private OrderRequestsMainDO orderRequestsMainDO;
    private OrderRequestsDO orderRequestsDO;
    private Context mContext;
    private String from, to;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public ActivityReportRequest(String frm, String tO, Context mContext) {

        this.mContext = mContext;
        this.from = frm;
        this.to = tO;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, OrderRequestsMainDO orderRequestsMainDO, String message);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");

        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.ACTIVITY_REPORT);
        String driverID = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YUSER", driverID);
            jsonObject.put("I_YSTARTDATE", from);
            jsonObject.put("I_YENDDATE", to);
//            jsonObject.put("I_YSTARTDATE", "20191028");
//            jsonObject.put("I_YENDDATE", "20191028");

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                String error = response.getProperty(0).toString();
                String[] messages = error.split("message=");
                error = messages[1].replace("]", "");
                error = error.replace("}", "");
                error = error.replace(";", "");
                error = error.replace(".", "");
                message = error;

                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("report xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            orderRequestsMainDO = new OrderRequestsMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        orderRequestsMainDO.orderRequestsDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        orderRequestsDO = new OrderRequestsDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {
                                orderRequestsMainDO.flag = Integer.parseInt(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_YREQNO")) {
                            if (text.length() > 0) {
                                orderRequestsDO.requestID = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YREQUSER")) {
                            if (text.length() > 0) {
                                orderRequestsDO.userID = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YREQUSERNAME")) {
                            if (text.length() > 0) {
                                orderRequestsDO.userName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YREQDATE")) {
                            if (text.length() > 0) {
                                orderRequestsDO.requestedDate = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YREQTIME")) {
                            if (text.length() > 0) {
                                orderRequestsDO.requestedTime = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YDOCTYPDES")) {
                            if (text.length() > 0) {
                                orderRequestsDO.requestedType = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPCODE")) {
                            if (text.length() > 0) {
                                orderRequestsDO.customer = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPDES")) {
                            if (text.length() > 0) {
                                orderRequestsDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSUPSTATUS")) {
                            if (text.length() > 0) {
                                orderRequestsDO.status = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YDOCSTATUS")) {
                            if (text.length() > 0) {
                                orderRequestsDO.docStatus = text;
                                if(orderRequestsDO.docStatus.equalsIgnoreCase("Approved")){
                                    orderRequestsDO.docStatus = text+"    ";
                                }else if(orderRequestsDO.docStatus.equalsIgnoreCase("Rejected")){
                                    orderRequestsDO.docStatus = text+"    ";
                                }
                                else {
                                    orderRequestsDO.docStatus = text;
                                }
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSUPLEVEL")) {
                            if (text.length() > 0) {
                                orderRequestsDO.signatureLevel = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCTYP")) {
                            if (text.length() > 0) {
                                orderRequestsDO.documentType = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YDOCNUM")) {
                            if (text.length() > 0) {
                                orderRequestsDO.deliveryNumber = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YACTQTY")) {
                            if (text.length() > 0) {
                                orderRequestsDO.actualQuantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YUPDQTY")) {
                            if (text.length() > 0) {
                                orderRequestsDO.updatedQuantity = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YACTPRI")) {
                            if (text.length() > 0) {
                                orderRequestsDO.actualPrice = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YUPDPRI")) {
                            if (text.length() > 0) {
                                orderRequestsDO.updatedPrice = Double.parseDouble(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITM")) {
                            if (text.length() > 0) {
                                orderRequestsDO.product = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                            if (text.length() > 0) {
                                orderRequestsDO.productDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSAU")) {
                            if (text.length() > 0) {
                                orderRequestsDO.unit = text;
                            }

                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        orderRequestsMainDO.orderRequestsDOS.add(orderRequestsDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            message = "Socket Time Out Exception Please try again";
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            message = " " + e;

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

//        ((BaseActivity)mContext).showLoader();
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
//        ((BaseActivity)mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, orderRequestsMainDO, message);
        }
    }
}