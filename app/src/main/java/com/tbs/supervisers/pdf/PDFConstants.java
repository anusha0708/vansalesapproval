package com.tbs.supervisers.pdf;

/*
 * Created by developer on 3/2/19.
 */
public interface PDFConstants {
    String ISO_LOGO_NAME = "iso_logo.png";
    String AUTHOR = "TBS";
    String CREATOR = "Venu Appasani";
    String HEADER_1 = "Brothers Gas Bottling & Distribution Co. LLC";
    String HEADER_2 = "Approval User Activity Report \n\n";
    String HEADER_3 = "                   ";
    String CYL_DELIVERY_NOTE_PDF_NAME = "ActivityReport.pdf";
    String CYL_NAME = "Brother's Gas - Activity Report";


    String USER = "User Name    ";

    String FROM = "From Date";
    String TO = "TO Date";
    String COLOUMN = ":";
    String DATE = "Print Date & Time";
    String FROM_TO = "From & To Date    : ";

}
