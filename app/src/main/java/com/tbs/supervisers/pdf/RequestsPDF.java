package com.tbs.supervisers.pdf;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.brothersgas.haadhir.utils.LogUtils;
import com.tbs.supervisers.activitys.BaseActivity;
import com.tbs.supervisers.model.OrderRequestsDO;
import com.tbs.supervisers.model.OrderRequestsMainDO;
import com.tbs.supervisers.utils.CalendarUtils;
import com.tbs.supervisers.utils.PreferenceUtils;
import com.tbs.supervisers.utils.Util;


import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/*
 * Created by developer on 25/1/19.
 */
public class RequestsPDF {

    private Context context;
    private OrderRequestsMainDO orderRequestsMainDO;
    private PdfPTable parentTable;
    private Font normalFont;
    private Font boldFont;
    private Document document;
    private LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap;
    String from, to;

    public RequestsPDF(Context context) {
        this.context = context;
    }


    public RequestsPDF createDeliveryPDF(String From, String To, LinkedHashMap<String, ArrayList<OrderRequestsDO>> orderRequestsDosMap, OrderRequestsMainDO activeDeliveryMainDO, String type) {
        this.orderRequestsMainDO = activeDeliveryMainDO;
        this.orderRequestsDosMap = orderRequestsDosMap;
        this.from                = From;
        this.to                  = To;
        boldFont                 = new Font(Font.FontFamily.COURIER, 9.0f, Font.BOLD, BaseColor.BLACK);
        normalFont               = new Font(Font.FontFamily.COURIER, 9.0f, Font.NORMAL, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Preview")) {
            normalFont = boldFont;
        }

        return this;
    }


    @SuppressLint("StaticFieldLeak")
    public class CreateDeliveryPDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Sending Activity Report please wait...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.cancel();
//            Intent intent1 = new Intent("preview");
//            LocalBroadcastManager.getInstance(context).sendBroadcast(intent1);
            String email = ((BaseActivity) context).preferenceUtils.getStringFromPreference
                    (PreferenceUtils.LOGIN_EMAIL, "");

            if (!email.isEmpty()) {
                PDFOperations.getInstance().sendpdfMail(context, email,
                        ((BaseActivity) context).preferenceUtils.getStringFromPreference
                                (PreferenceUtils.DRIVER_NAME, ""),
                        PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME, PDFConstants.CYL_NAME);
            }

        }
    }


    //Pre pare pdf
    public void preparePDF() {
        parentTable = new PdfPTable(1);
        parentTable.setWidthPercentage(100);
        initFile();
        addHeaderLabel();
//        addEmptySpaceLine(1);
        addDeliveryDetails();
//        addEmptySpaceLine(1);
        addProductsDetailsTableValues();
        document.close();
    }

    private void initFile() {
        File file = new File(Util.getAppPath(context) + PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
            document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor(PDFConstants.AUTHOR);
            document.addCreator(PDFConstants.CREATOR);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void addHeaderLabel() {
        try {
            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 20.0f, Font.BOLD, BaseColor.BLACK);

            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            document.add(headerLabel1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addDeliveryDetails() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(40f);
            headerParentTable.setWidthPercentage(100);
            headerParentTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            headerParentTable.getDefaultCell().setBorderColor(BaseColor.WHITE);

            float[] columnWidths = {2.2f, 0.3f, 4, 1.5f, 0.3f, 3f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellOne1 = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellThree1 = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            cellOne1.setBorder(Rectangle.NO_BORDER);
            cellTwo.setBorder(Rectangle.NO_BORDER);
            cellThree.setBorder(Rectangle.NO_BORDER);
            cellThree1.setBorder(Rectangle.NO_BORDER);
            cellFour.setBorder(Rectangle.NO_BORDER);
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);

            String userId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
            String userName = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "");

            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);

            Paragraph headerLabel1 = new Paragraph(PDFConstants.FROM_TO + from+" - "+to, headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_LEFT);
            Paragraph headerLabel2 = new Paragraph(PDFConstants.USER + "     : " + userId + " - " + userName, headerLabelFont);
            headerLabel2.setAlignment(Element.ALIGN_LEFT);

            Paragraph headerLabel3 = new Paragraph(PDFConstants.DATE + " : "+ CalendarUtils.getPDFDate()+" - "+CalendarUtils.getPDFTime(), headerLabelFont);
            headerLabel3.setAlignment(Element.ALIGN_LEFT);

            document.add(headerLabel2);
            document.add(headerLabel1);
            document.add(headerLabel3);
            addEmptySpaceLine(1);


            cellOne.addElement(new Phrase(PDFConstants.FROM, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.DATE, boldFont));


            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));


            cellTwo.addElement(new Phrase("22/02/2019", normalFont));
            cellTwo.addElement(new Phrase("22/02/2019 - 23:02:06", normalFont));


            cellThree.addElement(new Phrase(PDFConstants.TO, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER, boldFont));

            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellFour.addElement(new Phrase("22/02/2019", normalFont));
            cellFour.addElement(new Phrase(userId + "-" + userName, normalFont));

            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);
            headerParentTable.addCell(pdfPCell);
//            document.add(headerParentTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsDetailsTableValues() {
        try {
            float[] productColumnsWidth = {10f};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            productsTable.setPaddingTop(10f);

            if (orderRequestsMainDO.orderRequestsDOS != null && orderRequestsMainDO.orderRequestsDOS.size() > 0) {
                for (int i = 0; i < orderRequestsDosMap.size(); i++) {
                    ArrayList<String> keySet = new ArrayList<>(orderRequestsDosMap.keySet());
                    ArrayList<OrderRequestsDO> orderRequestsDos = orderRequestsDosMap.get(keySet.get(i));
                    OrderRequestsDO actDelDo = orderRequestsDos.get(0);
                    PdfPCell cellOne = new PdfPCell();
                    String from = actDelDo.requestedDate;
                    String aMonth = from.substring(4, 6);
                    String ayear = from.substring(0, 4);
                    String aDate = from.substring(Math.max(from.length() - 2, 0));

                    cellOne.setHorizontalAlignment(Element.ALIGN_LEFT);
                    if (actDelDo.documentType == 1) {

                        Paragraph prOne = new Paragraph(""
                                + "Customer     : " + actDelDo.customer + "-" + actDelDo.customerName + "\n"
                                + "Request      : " + actDelDo.requestID
                                + "                                Type : " + actDelDo.requestedType + "\n"
                                + "Date & Time  : " + aDate + "-" + aMonth + "-" + ayear + "  "
                                + actDelDo.requestedTime + "                              Status : " + actDelDo.status + "\n"
                                + "Final Status : " + actDelDo.docStatus + "(LOS-" + actDelDo.signatureLevel + ")"
                                + "                            Delivery No : " + actDelDo.deliveryNumber, normalFont);
                        prOne.setAlignment(Element.ALIGN_LEFT);
                        cellOne.addElement(prOne);
//                        PdfPTable headerParentTable = new PdfPTable(1);
//                        headerParentTable.setPaddingTop(10f);
//                        headerParentTable.setWidthPercentage(100);

                        float[] columnWidths = {4, 3, 3};
                        PdfPTable headerTable = new PdfPTable(columnWidths);
                        headerTable.setWidthPercentage(100);
                        PdfPCell one = new PdfPCell();
//                        one.setBorder(Rectangle.NO_BORDER);
                        PdfPCell two = new PdfPCell();
//                        two.setBorder(Rectangle.NO_BORDER);
                        PdfPCell three = new PdfPCell();
//                        three.setBorder(Rectangle.NO_BORDER);
                        one.addElement(new Phrase("Product", boldFont));
                        two.addElement(new Phrase("Actual Quantity", boldFont));
                        three.addElement(new Phrase("Updated Quantity", boldFont));
                        headerTable.addCell(one);
                        headerTable.addCell(two);
                        headerTable.addCell(three);
                        cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellOne.addElement(headerTable);


//                        productsTable.addCell(pdfPCell);
                        try {
                            for (int j = 0; j < orderRequestsDos.size(); j++) {
                                final OrderRequestsDO orderRequestsDO = orderRequestsDos.get(j);
                                float[] columnWidths2 = {4, 3, 3};
                                PdfPTable headerTable2 = new PdfPTable(columnWidths2);
                                headerTable2.setWidthPercentage(100);
                                PdfPCell one1 = new PdfPCell();
//                                one1.setBorder(Rectangle.NO_BORDER);
                                PdfPCell two1 = new PdfPCell();
//                                two1.setBorder(Rectangle.NO_BORDER);
                                PdfPCell three1 = new PdfPCell();
//                                three1.setBorder(Rectangle.NO_BORDER);
                                one1.addElement(new Phrase(orderRequestsDO.productDescription, normalFont));
                                two1.addElement(new Phrase("" + orderRequestsDO.actualQuantity + " " + orderRequestsDO.unit, normalFont));
                                three1.addElement(new Phrase("" + orderRequestsDO.updatedQuantity + " " + orderRequestsDO.unit, normalFont));
                                headerTable2.addCell(one1);
                                headerTable2.addCell(two1);
                                headerTable2.addCell(three1);
                                cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                                cellOne.addElement(headerTable2);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (actDelDo.documentType == 2) {
                        Paragraph prOne = new Paragraph(""
                                + "Customer     : " + actDelDo.customer + "-" + actDelDo.customerName + "\n"
                                + "Request      : " + actDelDo.requestID
                                + "                                Type : " + actDelDo.requestedType + "\n"
                                + "Date & Time  : " + aDate + "-" + aMonth + "-" + ayear + "  "
                                + actDelDo.requestedTime + "                              Status : " + actDelDo.status + "\n"
                                + "Final Status : " + actDelDo.docStatus + "(LOS-" + actDelDo.signatureLevel + ")"
                                + "                            Delivery No : " + actDelDo.deliveryNumber, normalFont);
                        prOne.setAlignment(Element.ALIGN_LEFT);
                        cellOne.addElement(prOne);
//                        addEmptySpaceLine(2);


                        float[] columnWidths = {4, 3, 3};
                        PdfPTable headerTable = new PdfPTable(columnWidths);
                        headerTable.setWidthPercentage(100);
                        PdfPCell one = new PdfPCell();
//                        one.setBorder(Rectangle.NO_BORDER);
                        PdfPCell two = new PdfPCell();
//                        two.setBorder(Rectangle.NO_BORDER);
                        PdfPCell three = new PdfPCell();
//                        three.setBorder(Rectangle.NO_BORDER);


                        Paragraph p1 = new Paragraph("Actual Price", normalFont);
                        p1.setAlignment(Element.ALIGN_RIGHT);
                        Paragraph p2 = new Paragraph("Updated Price", normalFont);
                        p2.setAlignment(Element.ALIGN_RIGHT);

                        one.addElement(new Phrase("Product", boldFont));
                        two.addElement(p1);
                        three.addElement(p2);
                        headerTable.addCell(one);
                        headerTable.addCell(two);
                        headerTable.addCell(three);
                        cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cellOne.addElement(headerTable);


//                        productsTable.addCell(pdfPCell);
                        try {
                            for (int j = 0; j < orderRequestsDos.size(); j++) {
                                final OrderRequestsDO orderRequestsDO = orderRequestsDos.get(j);
                                float[] columnWidths2 = {4, 3, 3};
                                PdfPTable headerTable2 = new PdfPTable(columnWidths2);
                                headerTable2.setWidthPercentage(100);
                                PdfPCell one1 = new PdfPCell();
//                                one1.setBorder(Rectangle.NO_BORDER);
                                PdfPCell two1 = new PdfPCell();
//                                two1.setBorder(Rectangle.NO_BORDER);
                                PdfPCell three1 = new PdfPCell();
//                                three1.setBorder(Rectangle.NO_BORDER);
                                two1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                                three1.setHorizontalAlignment(Element.ALIGN_RIGHT);

                                one1.addElement(new Phrase(orderRequestsDO.productDescription, normalFont));

                                Paragraph p4 = new Paragraph("" + orderRequestsDO.actualPrice + " AED ", normalFont);
                                p4.setAlignment(Element.ALIGN_RIGHT);
                                Paragraph p5 = new Paragraph("" + orderRequestsDO.updatedPrice + " AED ", normalFont);
                                p5.setAlignment(Element.ALIGN_RIGHT);

                                two1.addElement(p4);
                                three1.addElement(p5);

                                headerTable2.addCell(one1);
                                headerTable2.addCell(two1);
                                headerTable2.addCell(three1);
                                cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                                cellOne.addElement(headerTable2);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else {
                        Paragraph prOne = new Paragraph(""
                                + "Customer     : " + actDelDo.customer + "-" + actDelDo.customerName + "\n"
                                + "Request      : " + actDelDo.requestID
                                + "                                Type : " + actDelDo.requestedType + "\n"
                                + "Date & Time  : " + aDate + "-" + aMonth + "-" + ayear + "  "
                                + actDelDo.requestedTime + "                              Status : " + actDelDo.status + "\n"
                                + "Final Status : " + actDelDo.docStatus + "(LOS-" + actDelDo.signatureLevel + ")"
                                + "                            Delivery No : " + actDelDo.deliveryNumber, normalFont);
                        prOne.setAlignment(Element.ALIGN_LEFT);
                        cellOne.addElement(prOne);
                    }

                    cellOne.addElement(new Phrase("\n", normalFont));

                    productsTable.addCell(cellOne);
//                    addEmptySpaceLine(1);

                }
                document.add(productsTable);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
