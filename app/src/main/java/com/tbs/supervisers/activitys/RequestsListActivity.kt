package com.tbs.supervisers.activitys

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.view.*
import android.widget.*
import com.tbs.supervisers.R
import com.tbs.supervisers.adapters.RequestsAdapter
import com.tbs.supervisers.model.OrderRequestsDO
import com.tbs.supervisers.utils.PreferenceUtils
import com.tbs.supervisers.utils.Util
import com.tbs.supervisers.webaccess.OrderRequestListRequest
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.tbs.supervisers.utils.WrapperLinearManager
import java.lang.Exception
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap


//
public class RequestsListActivity : BaseActivity() {

    public lateinit var loanReturnAdapter: RequestsAdapter
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView


    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.booking_list_layout, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvScreenTitle.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""))
//        ivRefresh.visibility=View.VISIBLE
//        ivRefresh.setOnClickListener {
//            getData()
//        }
        ivMenu.visibility = View.VISIBLE

        ivMenu.setOnClickListener {

            val popup = PopupMenu(this@RequestsListActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.superviser_menu, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Users")) {


                    } else if (item.title.equals("Activity Report")) {
                        val intent = Intent(this@RequestsListActivity, ActivityReportActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)

                    }
                    if (item.title.equals("Logout")) {
                        hideLoader()
                        logOut()
                    } else if (item.title.equals("Sync")) {
                        hideLoader()
                        getData()
                    }

                    return true

                }


            })
            popup.show()

        }
        initializeControls()
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                 IntentFilter("custom-event-name"));

    }
    public var mMessageReceiver = object: BroadcastReceiver() {
        override fun onReceive(context:Context, intent:Intent) {
            hideLoader()
            getData()


        }
    }

    override fun initializeControls() {

        recycleview = findViewById(R.id.rvOrderList) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = findViewById(R.id.tvNoOrders) as TextView

        val linearLayoutManager = WrapperLinearManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.setLayoutManager(linearLayoutManager)
        recycleview.setNestedScrollingEnabled(false);

        loanReturnAdapter = RequestsAdapter(this@RequestsListActivity, LinkedHashMap(), ArrayList())
//        var animator = recycleview.getItemAnimator()
//        if (animator is SimpleItemAnimator) {
//            (animator as SimpleItemAnimator).supportsChangeAnimations = false
//        }
//        recycleview.getItemAnimator()!!.endAnimations();
//        showLoader()
        try {
            hideLoader()
            getData()

        } catch (e: Exception) {

        }


    }

    fun getData() {
        //orderRequestsDOSMap.clear()
        //orderDOS.clear()

//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);


        if (Util.isNetworkAvailable(this)) {
            showLoader()
            val requestList = OrderRequestListRequest(this@RequestsListActivity)
            requestList.setOnResultListener { isError, orderRequestsMainDO, message ->
                if (isError) {
                    hideLoader()

                    recycleview.setVisibility(View.GONE)
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    tvNoDataFound.setText("" + message)
//                    refresh()
                } else {

                    var orderRequestsDOSMap = LinkedHashMap<String, ArrayList<OrderRequestsDO>>();
                    var orderDOS = ArrayList<OrderRequestsDO>()
                    var availableStockDos: ArrayList<OrderRequestsDO> = ArrayList()
                    var requestDos = ArrayList<String>()

//                    val orderRequestsDOSMap = LinkedHashMap<String, ArrayList<OrderRequestsDO>>();
//                    var requestDos = ArrayList<String>()
                    if (orderRequestsMainDO != null && orderRequestsMainDO.orderRequestsDOS.size > 0) {
                        for (i in orderRequestsMainDO.orderRequestsDOS.indices) {
                            if (!requestDos.contains(orderRequestsMainDO.orderRequestsDOS.get(i).requestID)) {
                                requestDos.add(orderRequestsMainDO.orderRequestsDOS.get(i).requestID)
                                availableStockDos.add(orderRequestsMainDO.orderRequestsDOS.get(i))
                            }
                        }
                    }

                    if (requestDos.size > 0) {

                        orderDOS.clear()

                        for (j in requestDos.indices) {//4
                            orderDOS = ArrayList<OrderRequestsDO>()
                            for (i in orderRequestsMainDO.orderRequestsDOS.indices) {//10
                                if (orderRequestsMainDO.orderRequestsDOS.get(i).requestID.equals(requestDos.get(j))) {
                                    orderDOS.add(orderRequestsMainDO.orderRequestsDOS.get(i))
                                }
                            }
                            orderRequestsDOSMap.put(requestDos.get(j), orderDOS);
                        }
                    }

                    if (orderRequestsDOSMap.size > 0) {
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)


                        loanReturnAdapter = RequestsAdapter(this@RequestsListActivity, orderRequestsDOSMap, availableStockDos)
                        recycleview.adapter = loanReturnAdapter
//                        StorageManager.getInstance(this).saveRequestsList(this, availableStockDos);
                        hideLoader()

                    } else {
                        tvNoDataFound.setVisibility(View.VISIBLE)
                        tvNoDataFound.setText("No Requests Found at the moment")
                        recycleview.setVisibility(View.GONE)
                        hideLoader()

                    }


                }
            }
            requestList.execute()

        } else {
            tvNoDataFound.setVisibility(View.VISIBLE)

            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {
            val intent = Intent();
            intent.putExtra("CapturedReturns", true)
            // AppConstants.CapturedReturns= true
            setResult(11, intent)
            finish()
        }


    }

    override fun onResume() {
        hideLoader()

        super.onResume()
    }

//    fun refresh() {
//
//        Handler().postDelayed({
//            hideLoader()
//            try {
//                runOnUiThread(Runnable { getRefreshData() })
//
//
//            } catch (e: Exception) {
//
//            }
////            getData()
//
//        }, 20000)
//    }

//    fun getRefreshData() {
//        //orderRequestsDOSMap.clear()
////        requestDos.clear()
////        availableStockDos.clear()
//        //orderDOS.clear()
//
//
//        if (Util.isNetworkAvailable(this)) {
//
//            val requestList = OrderRequestListRequest(this@RequestsListActivity)
//            requestList.setOnResultListener { isError, orderRequestsMainDO, message ->
//                hideLoader()
//                if (isError) {
//                    tvNoDataFound.setVisibility(View.VISIBLE)
//                    tvNoDataFound.setText("" + AppConstants.message)
//                    recycleview.setVisibility(View.GONE)
////                    refresh()
//                } else {
//                    var orderRequestsDOSMap = LinkedHashMap<String, ArrayList<OrderRequestsDO>>();
//                    var orderDOS = ArrayList<OrderRequestsDO>()
//                    var requestDos = ArrayList<String>()
//                    var availableStockDos: ArrayList<OrderRequestsDO> = ArrayList()
//
////                    val orderRequestsDOSMap = LinkedHashMap<String, ArrayList<OrderRequestsDO>>();
////                    var requestDos = ArrayList<String>()
//                    if (orderRequestsMainDO != null && orderRequestsMainDO.orderRequestsDOS.size > 0) {
//                        for (i in orderRequestsMainDO.orderRequestsDOS.indices) {
//                            if (!requestDos.contains(orderRequestsMainDO.orderRequestsDOS.get(i).requestID)) {
//                                requestDos.add(orderRequestsMainDO.orderRequestsDOS.get(i).requestID)
//                                availableStockDos.add(orderRequestsMainDO.orderRequestsDOS.get(i))
//                            }
//                        }
//                    }
//
//                    if (requestDos.size > 0) {
//                        orderDOS.clear()
//
//                        for (j in requestDos.indices) {//4
//                            orderDOS = ArrayList<OrderRequestsDO>()
//                            for (i in orderRequestsMainDO.orderRequestsDOS.indices) {//10
//                                if (orderRequestsMainDO.orderRequestsDOS.get(i).requestID.equals(requestDos.get(j))) {
//                                    orderDOS.add(orderRequestsMainDO.orderRequestsDOS.get(i))
//                                }
//                            }
//                            orderRequestsDOSMap.put(requestDos.get(j), orderDOS);
//                        }
//                    }
//
//                    if (orderRequestsDOSMap.size > 0) {
//                        tvNoDataFound.setVisibility(View.GONE)
//                        recycleview.setVisibility(View.VISIBLE)
//
//                        var localDos = StorageManager.getInstance(this).getRequestsList(this)
//
//                        var currentDOS = compareList(localDos, availableStockDos)
//
//                        if (currentDOS == true) {
//                            showLoader()
//                            showToast("You have new Request")
//
//                            val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                                v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
//                            } else {
//                                v.vibrate(500)
//                            }
//
//                            loanReturnAdapter.refreshAdapter(this, orderRequestsDOSMap, availableStockDos)
//                            StorageManager.getInstance(this).saveRequestsList(this, availableStockDos);
//                            hideLoader()
//
//                        } else {
////                            showToast("No new Request")
//                        }
////                        refresh()
//                    } else {
//                        tvNoDataFound.setVisibility(View.VISIBLE)
//                        tvNoDataFound.setText("No Requests Found at the moment")
//                        recycleview.setVisibility(View.GONE)
////                        refresh()
//                    }
//
//
//                }
//            }
//            requestList.execute()
//
//        } else {
//            tvNoDataFound.setVisibility(View.VISIBLE)
//
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
//
//        }
//        //refresh()
//    }

    override fun onDestroy() {

        super.onDestroy()
    }

    fun compareList(localList: ArrayList<OrderRequestsDO>, dataList: ArrayList<OrderRequestsDO>): Boolean {
//        var loopedList = java.util.ArrayList<OrderRequestsDO>()
        var isProductExisted = false;
        for (i in localList.indices) {
            for (k in dataList!!.indices) {
                if (localList.get(i).requestID.equals(dataList!!.get(k).requestID, true)) {
                    isProductExisted = true
                } else {
                    isProductExisted = false

                }
            }

        }
        if (isProductExisted) {
            isProductExisted = false

        } else {
            isProductExisted = true

        }
        return isProductExisted
    }


}
