package com.tbs.supervisers.activitys

import android.content.Intent
import android.net.Uri
import androidx.drawerlayout.widget.DrawerLayout
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.supervisers.R
import com.tbs.supervisers.model.LoginDO
import com.tbs.supervisers.utils.AppPrefs
import com.tbs.supervisers.utils.CalendarUtils
import com.tbs.supervisers.utils.PreferenceUtils
import com.tbs.supervisers.utils.Util
import com.tbs.supervisers.webaccess.AdminLoginRequest
import com.tbs.supervisers.webaccess.LoginMultipleUsersRequest
import com.tbs.supervisers.webaccess.LoginTimeCaptureRequest

import java.text.SimpleDateFormat
import java.util.*

class LoginActivity : BaseActivity() {
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String
    internal lateinit var pdbUser: String
    internal lateinit var pdbPass: String
    lateinit var llSplash: ScrollView
    internal lateinit var companyId: String

    override fun initializeControls() {
    }

    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.activity_login, null) as ScrollView
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

        val btnLogin = findViewById(R.id.btnLogin) as Button
        val etUserName = findViewById(R.id.etUserName) as EditText
        val etPassword = findViewById(R.id.etPassword) as EditText
        val etCompanyId = findViewById(R.id.etCompanyId) as EditText

        val tvCopyRight = findViewById(R.id.tvCopyRight) as TextView

        etCompanyId.setText("" + resources.getString(R.string.company_name))
        etUserName.requestFocus()
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.CONFIG_SUCCESS, "").isEmpty()) {
            preferenceUtils.saveString(PreferenceUtils.IP_ADDRESS, "47.91.104.128")
            preferenceUtils.saveString(PreferenceUtils.PORT, "8124")
            preferenceUtils.saveString(PreferenceUtils.ALIAS, "BROSGAS")
            preferenceUtils.saveString(PreferenceUtils.A_USER_NAME, "VANSALES")
            preferenceUtils.saveString(PreferenceUtils.A_PASSWORD, "12345")
        }
        btnLogin.setOnClickListener {
            dbUser = etUserName!!.text.toString().trim()
            dbPass = etPassword!!.text.toString().trim()
            companyId = etCompanyId!!.text.toString().trim()
            if (companyId.length == 0) {
                showAlert("" + resources.getString(R.string.login_company_id))
            } else if (dbUser.length == 0) {
                showAlert("" + resources.getString(R.string.login_username))
            } else if (dbPass.length == 0) {
                showAlert("" + resources.getString(R.string.login_password))
            }
//            else if (isExpired()) {
//                showAppCompatAlert("" + resources.getString(R.string.login_info),
//                        "" + resources.getString(R.string.login_licence),
//                        "" + resources.getString(R.string.ok), "",
//                        "" + resources.getString(R.string.from_expired), false);
//            }
            else {
                loginSuccess()
            }

        }


        val ivConfiguration = findViewById(R.id.ivConfiguration) as ImageView
        val imageView = findViewById(R.id.imageView) as ImageView
        var count = 0

        imageView.setOnClickListener {
            count = count + 1
            if (count > 9) {

                val intent = Intent(this@LoginActivity, ConfigurationActivity::class.java);
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent);
                finish()
            } else {
//                showToast("Count : "+count)
//                imageView.isClickable=false
//                imageView.isEnabled=false
            }

        }
//
//        ivConfiguration.setOnClickListener {
//            val intent = Intent(this@LoginActivity, ConfigurationActivity::class.java);
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            startActivity(intent);
//            finish()
//        }
    }

    override fun onButtonYesClick(from: String) {
        if (from.equals(resources.getString(R.string.from_expired), true)) {

        }
        if (from.equals("PLAYSTORE", true)) {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }

    }

    private fun isExpired(): Boolean {
        try {
            val dateFormat = "dd/MM/yyyy"
            val expireDate = "31/12/2022"
            val sdf = SimpleDateFormat(dateFormat, Locale.getDefault()) // 28-01-2019
            val today = getToday(dateFormat)
            val eDate = sdf.parse(expireDate)
            val curDate = sdf.parse(today)
            if (eDate.compareTo(curDate) <= 0) {
                return true;
            }

        } catch (e: Exception) {
            e.printStackTrace()

        }
        return false
    }

    private fun getToday(format: String): String {
        val date = Date()
        return SimpleDateFormat(format).format(date)
    }

    private fun loginSuccess() {
        if (Util.isNetworkAvailable(this)) {
            val adminLoginRequest = AdminLoginRequest(this@LoginActivity)
            adminLoginRequest.setOnResultListener(object : AdminLoginRequest.OnResultListener {
                override fun onCompleted(isError: Boolean, isValid: Boolean, loginDO: LoginDO) {
                    if (isError) {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        } else {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        }
                    } else {
                        if (loginDO.flag == 20) {

                            preferenceUtils = PreferenceUtils(this@LoginActivity)
                            preferenceUtils.saveString(PreferenceUtils.USER_NAME, dbUser)
                            preferenceUtils.saveString(PreferenceUtils.PASSWORD, dbPass)
                            preferenceUtils.saveString(PreferenceUtils.PROFILE_PICTURE, loginDO.Image)
                            preferenceUtils.saveString(PreferenceUtils.DRIVER_ID, loginDO.driverCode)
                            preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, loginDO.site)
                            preferenceUtils.saveString(PreferenceUtils.LOGIN_DRIVER_NAME, loginDO.driverName)
                            preferenceUtils.saveString(PreferenceUtils.B_SITE_NAME, loginDO.siteDescription)
                            preferenceUtils.saveString(PreferenceUtils.COMPANY, loginDO.company)
                            preferenceUtils.saveString(PreferenceUtils.COMPANY_DES, loginDO.companyDescription)
                            preferenceUtils.saveString(PreferenceUtils.COMPANY, companyId)
                            preferenceUtils.saveString(PreferenceUtils.LOGIN_EMAIL, loginDO.email)
                            preferenceUtils.saveString(PreferenceUtils.APP_VERSION, loginDO.version)
                            preferenceUtils.saveInt(PreferenceUtils.USER_ROLE, loginDO.role)
                            preferenceUtils.saveInt(PreferenceUtils.CHEQUE_DATE, loginDO.chequeDays)
                            preferenceUtils.saveInt(PreferenceUtils.USER_AUTHORIZED, loginDO.authorized)

                            preferenceUtils.saveString(PreferenceUtils.LOGIN_SUCCESS, "" + resources.getString(R.string.from_success))
                            loginTimeCapture()

                            val intent = Intent(this@LoginActivity, RequestsListActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

                            startActivity(intent)
                            finish()


                        } else {
                            showAlert(loginDO.message)
                        }
                    }
                }
            })
            adminLoginRequest.execute(dbUser, dbPass)
        } else {
            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.internet_connection), "" + resources.getString(R.string.ok), "", "", false)

        }
    }

    private fun loginTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = LoginTimeCaptureRequest(driverId, date, date, time, this@LoginActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
//                    Toast.makeText(this@LoginActivity, "Failed", Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 1) {
//                       showToast("Success")

                        } else {
//                        showToast("Success")

                        }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), ""+resources.getString(R.string.ok), "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }


    }


    override fun onBackPressed() {
    }
}