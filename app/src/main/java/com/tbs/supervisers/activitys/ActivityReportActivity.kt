package com.tbs.supervisers.activitys

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.shockwave.pdfium.PdfDocument
import com.tbs.supervisers.R
import com.tbs.supervisers.adapters.ReportAdapter
import com.tbs.supervisers.model.OrderRequestsDO
import com.tbs.supervisers.model.OrderRequestsMainDO
import com.tbs.supervisers.pdf.PDFConstants
import com.tbs.supervisers.pdf.PDFOperations
import com.tbs.supervisers.pdf.RequestsPDF
import com.tbs.supervisers.utils.CalendarUtils
import com.tbs.supervisers.utils.Util
import com.tbs.supervisers.webaccess.ActivityReportRequest
import java.io.File

import java.util.*
import kotlin.collections.ArrayList


class ActivityReportActivity : BaseActivity(), OnPageChangeListener, OnLoadCompleteListener {


    lateinit var btnCreate: Button
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var orderRequestsDosMap: LinkedHashMap<String, java.util.ArrayList<OrderRequestsDO>>

    lateinit var tvNoDataFound: TextView
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""
    lateinit var orderRequestsMainDO: OrderRequestsMainDO
    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var tvFrom: TextView
    lateinit var tvTO: TextView
    lateinit var pdfView: PDFView
    lateinit var ll1: LinearLayout
    internal var pageNumber: Int? = 0
    internal var pdfFileName: String? = null
    internal var TAG = "PdfActivity"
    var fileList = java.util.ArrayList<File>()
    lateinit var dir: File

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.transactions_list, null) as View
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        ivMenu.visibility = View.GONE
        insertDummyContactWrapper()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.setText("Activity Report")
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter("preview"));

        tvNoDataFound = findViewById(R.id.tvNoData) as TextView
        tvFrom = findViewById(R.id.tvFrom) as TextView
        tvTO = findViewById(R.id.tvTO) as TextView
        pdfView = findViewById(R.id.pdfView) as PDFView
        var from = CalendarUtils.getDate()
        val aMonth = from.substring(4, 6)
        val ayear = from.substring(0, 4)
        val aDate = from.substring(Math.max(from.length - 2, 0))
        tvFrom.setText("" + aDate + " - " + aMonth + " - " + ayear)
        tvTO.setText("" + aDate + " - " + aMonth + " - " + ayear)
        startDate = ayear + aMonth + aDate
        endDate = ayear + aMonth + aDate
        recycleview = findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview.setLayoutManager(linearLayoutManager)
        btnCreate = findViewById(R.id.btnCreate) as Button
        val btnSearch = findViewById(R.id.btnSearch) as Button
        btnSearch.setOnClickListener {

            var frm = tvFrom.text.toString().trim();
            var to = tvTO.text.toString().trim()
            if (frm.isNotEmpty() && to.isNotEmpty()) {
                getData(startDate, endDate)

            } else {
                showToast("Please select From and To dates")
            }
        }
        val c = Calendar.getInstance()
        val cyear  = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday   = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(this@ActivityReportActivity, datePickerListener, cyear, cmonth, cday)
            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(this@ActivityReportActivity, endDatePickerListener, cyear, cmonth, cday)
            datePicker.show()
        }
        btnCreate.setOnClickListener {
            RequestsPDF(this).createDeliveryPDF(tvFrom.text.toString().replace(" - ","/"),tvTO.text.toString().replace(" - ","/"),orderRequestsDosMap, orderRequestsMainDO, "Preview").CreateDeliveryPDF().execute()
            btnCreate.isClickable = false
            btnCreate.isEnabled = false
            btnCreate.setBackgroundColor(resources.getColor(R.color.md_gray_light))
        }
    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString;
        tvFrom.setText("" + dayString + " - " + monthString + " - " + cyear)
        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString;
        tvTO.setText("" + dayString + " - " + monthString + " - " + cyear)
        to = tvTO.text.toString()

    }

    fun getData(from: String, to: String) {
        if (Util.isNetworkAvailable(this)) {
            showLoader()
            val requestList = ActivityReportRequest(from, to, this@ActivityReportActivity)
            requestList.setOnResultListener { isError, orderRequestsMainDo, message ->
                if (isError) {
                    hideLoader()
                    pdfView.visibility = View.GONE
                    tvNoDataFound.setVisibility(View.VISIBLE)
                    tvNoDataFound.setText("" + message)
                    btnCreate.visibility = View.GONE
                } else {
                    hideLoader()
                    orderRequestsMainDO= OrderRequestsMainDO()
                    orderRequestsMainDO.orderRequestsDOS.clear()
                    orderRequestsDosMap = LinkedHashMap<String, ArrayList<OrderRequestsDO>>();
                    orderRequestsDosMap.clear()
                    orderRequestsMainDO = orderRequestsMainDo
                    var orderDOS = ArrayList<OrderRequestsDO>()
                    var availableStockDos: ArrayList<OrderRequestsDO> = ArrayList()
                    var requestDos = ArrayList<String>()

                    if (orderRequestsMainDO != null && orderRequestsMainDO.orderRequestsDOS.size > 0) {
                        for (i in orderRequestsMainDO.orderRequestsDOS.indices) {
                            if (!requestDos.contains(orderRequestsMainDO.orderRequestsDOS.get(i).requestID)) {
                                requestDos.add(orderRequestsMainDO.orderRequestsDOS.get(i).requestID)
                                availableStockDos.add(orderRequestsMainDO.orderRequestsDOS.get(i))
                            }
                        }
                    }

                    if (requestDos.size > 0) {

                        orderDOS.clear()

                        for (j in requestDos.indices) {//4
                            orderDOS = ArrayList<OrderRequestsDO>()
                            for (i in orderRequestsMainDO.orderRequestsDOS.indices) {//10
                                if (orderRequestsMainDO.orderRequestsDOS.get(i).requestID.equals(requestDos.get(j))) {
                                    orderDOS.add(orderRequestsMainDO.orderRequestsDOS.get(i))
                                }
                            }
                            orderRequestsDosMap.put(requestDos.get(j), orderDOS);
                        }
                    }

                    if (orderRequestsDosMap.size > 0) {
                        tvNoDataFound.setVisibility(View.GONE)
                        recycleview.setVisibility(View.VISIBLE)

                        orderRequestsMainDO = orderRequestsMainDo
                        tvNoDataFound.visibility = View.GONE
                        var adapter = ReportAdapter(this@ActivityReportActivity, orderRequestsDosMap, availableStockDos)
                        recycleview.setAdapter(adapter)
//                        pdfView.visibility=View.VISIBLE
                        btnCreate.visibility = View.VISIBLE
                        btnCreate.isClickable = true
                        btnCreate.isEnabled = true
                        btnCreate.setBackgroundColor(resources.getColor(R.color.md_green))

                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        pdfView.visibility = View.GONE
                        btnCreate.visibility = View.GONE

                    }

                }
            }
            requestList.execute()

        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)
        }


    }

    public var mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            hideLoader()
            btnCreate.visibility = View.VISIBLE
            init()
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun insertDummyContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera")
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location")
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    123)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    private fun init() {

        //        position = getIntent().getIntExtra("position",-1);
        displayFromSdcard()
    }

    private fun displayFromSdcard() {

        pdfFileName = PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME;
        dir = File(Util.getAppPath(this@ActivityReportActivity))

        getfile(dir)

        //        File file= new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath().endsWith(pdfFileName)));
        for (j in fileList.indices) {
            if (fileList[j].absoluteFile.name == pdfFileName) {
                pdfView.fromFile(fileList[j].absoluteFile)
                        .defaultPage(pageNumber!!)
                        .enableSwipe(true)

                        .swipeHorizontal(false)
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(DefaultScrollHandle(this))
                        .load()
            }
        }


    }


    fun getfile(dir: File): java.util.ArrayList<File> {

        val listFile = dir.listFiles()
        var i = 0
        if (listFile != null && listFile.size > 0) {
            i = 0
            while (i < listFile.size) {

                if (listFile[i].isDirectory) {
                    getfile(listFile[i])

                } else {

                    var booleanpdf = false
                    if (listFile[i].name.endsWith(pdfFileName!!)) {

                        for (j in fileList.indices) {
                            if (fileList[j].name == listFile[i].name) {
                                booleanpdf = true
                            } else {

                            }
                        }

                        if (booleanpdf) {
                            booleanpdf = false
                        } else {
                            fileList.add(listFile[i])

                        }
                    }
                }
                i++
            }
        }
        return fileList
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        pageNumber = page
        title = String.format("%s %s / %s", pdfFileName, page + 1, pageCount)
    }


    override fun loadComplete(nbPages: Int) {
        val meta = pdfView.documentMeta
        printBookmarksTree(pdfView.tableOfContents, "-")

    }

    fun printBookmarksTree(tree: List<PdfDocument.Bookmark>, sep: String) {
        for (b in tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.title, b.pageIdx))

            if (b.hasChildren()) {
                printBookmarksTree(b.children, "$sep-")
            }
        }
    }
}