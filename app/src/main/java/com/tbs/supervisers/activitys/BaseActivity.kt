package com.tbs.supervisers.activitys

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.os.Bundle
import android.os.storage.StorageManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.tbs.brothersgas.haadhir.utils.*
import com.tbs.supervisers.R
import com.tbs.supervisers.common.AppConstants
import com.tbs.supervisers.utils.CalendarUtils
import com.tbs.supervisers.utils.PreferenceUtils
import com.tbs.supervisers.webaccess.LogoutTimeCaptureRequest
import java.util.*


abstract class BaseActivity : AppCompatActivity() {

    lateinit var ivMenu: ImageView

    lateinit var toolbar: Toolbar
    lateinit var llBody: LinearLayout


    private lateinit var dialogLoader: Dialog
    private var number = ""


    lateinit var mDrawerLayout: androidx.drawerlayout.widget.DrawerLayout
    private var context: Context? = null

    lateinit var flToolbar: FrameLayout


    lateinit var preferenceUtils: PreferenceUtils

    //    lateinit   var ivBack: ImageView
    lateinit var tvScreenTitle: TextView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.base_layout)

        context = this@BaseActivity
        //        inflater = this.getLayoutInflater();

        baseInitializeControls()

        preferenceUtils = PreferenceUtils(context)


        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }


        initialize()

        setTypeFace(AppConstants.MONTSERRAT_REGULAR_TYPE_FACE, llBody)
    }
    /*} catch (JSONException e) {
            e.printStackTrace();
        }*/

    fun showLoader() {
        runOnUiThread(RunShowLoader("Loading..."))
    }

    //Method to show loader with text
    fun showLoader(msg: String) {
        runOnUiThread(RunShowLoader(msg))
    }

    fun showToast(strMsg: String) {
        if (!strMsg.isEmpty()) {
            Toast.makeText(this@BaseActivity, strMsg, Toast.LENGTH_SHORT).show()
        }
    }

    fun showAlert(strMsg: String) {
        if (!strMsg.isEmpty()) {
            showAppCompatAlert("Alert!", strMsg, "OK", "", "", true)
        }
    }

    fun showSnackbar(errorMsg: String) {

        val snackbar = Snackbar.make(llBody, errorMsg, Snackbar.LENGTH_SHORT)
        snackbar.show()

    }

    var alertDialog: AlertDialog.Builder? = null
    fun showAppCompatAlert(mTitle: String, mMessage: String, posButton: String, negButton: String?, from: String, isCancelable: Boolean) {
        if (alertDialog == null)
            alertDialog = AlertDialog.Builder(this@BaseActivity, R.style.AppCompatAlertDialogStyle)
        alertDialog!!.setTitle(mTitle)
        alertDialog!!.setMessage(mMessage)
        number = mMessage
        alertDialog!!.setPositiveButton(posButton) { dialog, which -> onButtonYesClick(from) }
        if (negButton != null && !negButton.equals("", ignoreCase = true))
            alertDialog!!.setNegativeButton(negButton) { dialog, which -> onButtonNoClick(from) }
        alertDialog!!.setCancelable(false)
        alertDialog!!.show()
    }

    open fun onButtonNoClick(from: String) {

    }

    open fun onButtonYesClick(from: String) {
//
//        if ("LOGOUT".equals(from, ignoreCase = true)) {
//
//            if (context !is LoginActivity) {
//             //   preferenceUtils.saveBoolean(PreferenceUtils.IS_LOGIN, false)
//                val logoutIntent = Intent(context, LoginActivity::class.java)
//                logoutIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(logoutIntent)
//                finish()
//            }
//        }

    }

    /**
     * For hiding progress dialog (Loader ).
     */

    fun hideLoader() {
        runOnUiThread {
            try {
                if (dialogLoader != null && dialogLoader.isShowing())
                    dialogLoader.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun hideKeyBoard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }


    private inner class RunShowLoader(private val strMsg: String) : Runnable {

        override fun run() {
            try {
                dialogLoader = Dialog(this@BaseActivity)
                dialogLoader.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogLoader.setCancelable(false)
                dialogLoader.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialogLoader.setContentView(R.layout.loader_custom)
                if (!dialogLoader.isShowing())
                    dialogLoader.show()
            } catch (e: Exception) {
                // dialogLoader = null
            }

        }
    }

    fun menuOperates() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT)
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT)
        }
    }


    protected fun disableMenuWithBackButton() {
        mDrawerLayout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
    }

    abstract fun initialize()

    abstract fun initializeControls()

    private fun baseInitializeControls() {
        flToolbar = findViewById<View>(R.id.flToolbar) as FrameLayout
        llBody = findViewById<View>(R.id.llBody) as LinearLayout
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        ivMenu = findViewById<View>(R.id.ivMenu) as ImageView
        tvScreenTitle = findViewById<View>(R.id.tvScreenTitle) as TextView

//        llUserProfile.setOnClickListener {
//            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
//            if (data.length > 0) {
//                if (llData.getVisibility() == View.VISIBLE) {
//                    llData.setVisibility(View.GONE);
//                    ivProfileArrow.setRotation(270f)
//                } else {
//                    llData.setVisibility(View.VISIBLE);
//                    ivProfileArrow.setRotation(90f)
//                }
//            } else {
//                tvRoutingId.setVisibility(View.GONE)
//                tvDate.setVisibility(View.GONE)
//                llData.setVisibility(View.GONE)
//
//            }
//        }

        ivMenu.setOnClickListener {

            val popup = PopupMenu(this@BaseActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.logout_menu, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Logout")) {
                        hideLoader()
                        logOut()


                    }

                    return true

                }


            })
            popup.show()//showing popup menu

        }

//        ivBack.setOnClickListener {
//           finish()
//        }

    }





    fun applyFont(root: View, font: String) {
        try {
            val fontName = fontStyleName(font)
            if (root is ViewGroup) {
                for (i in 0 until root.childCount)
                    applyFont(root.getChildAt(i), fontName)
            } else if (root is TextView) {
                root.typeface = Typeface.createFromAsset(assets, fontName)
            } else if (root is EditText) {
                root.typeface = Typeface.createFromAsset(assets, fontName)
            }
        } catch (e: Exception) {
            LogUtils.error("HAADHIR", String.format("Error occured when trying to apply %s font for %s view", font, root))
            e.printStackTrace()
        }

    }

    private fun fontStyleName(name: String): String {
        var fontStyleName = ""
        when (name) {
            AppConstants.Goudy_Old_Style_Italic -> fontStyleName = "gillsansstd.otf"
            AppConstants.Goudy_Old_Style_Normal -> fontStyleName = "montserrat_regular.ttf"
            AppConstants.Goudy_Old_Style_Bold -> fontStyleName = "montserrat_bold.ttf"

            "" -> fontStyleName = "montserrat_regular.ttf"
        }
        return fontStyleName
    }


    override fun onBackPressed() {
        hideLoader()
        finish()
    }

    public fun logOut() {




        logOutTimeCapture()


    }
    private fun logOutTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()

        val siteListRequest = LogoutTimeCaptureRequest(driverId, date, date, time, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            hideLoader()
            if (loginDo != null) {
                if (isError) {
                    hideLoader()
                    showToast("you are unable to logout")
                } else {
                    hideLoader()
                    if (loginDo.flag == 20) {
                        showToast("You have successfully logged out!")
                        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID)
                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS)
                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_EMAIL)

                        val intent = Intent(this@BaseActivity, LoginActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)

                    } else {
                        showToast("you are unable to logout")

                    }

                }
            } else {
                hideLoader()
            }

        }

        siteListRequest.execute()
    }


    override fun onResume() {

        super.onResume()
    }

    private fun checkDateAndTime() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Setting Alert")
        builder.setMessage("Your phone date is inaccurate! Adjust your clock and try again")
        builder.setCancelable(false)
        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
        builder.setPositiveButton("Adjust Date") { dialog, which ->
            startActivityForResult(Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
        }
        builder.show()


    }

    public fun setTypeFace(fontType: String, viewGroup: ViewGroup) {
        val count = viewGroup.getChildCount()
        var v: View
        for (i in 0 until count) {
            v = viewGroup.getChildAt(i)
            if (v is TextView || v is Button || v is EditText || v is TextInputEditText)
                (v as TextView).typeface = getTypeFace(fontType)

            else if (v is ViewGroup)
                setTypeFace(fontType, v)
        }

    }

    private fun getTypeFace(fontType: String): Typeface {
        return Typeface.createFromAsset(getAssets(), fontType);
//        if(fontType.equals(AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)){
//
//        }
//        else if(fontType.equals(AppConstants.MONTSERRAT_MEDIUM_TYPE_FACE)){
//
//        }
//        else if(fontType.equals(AppConstants.MONTSERRAT_BOLD_TYPE_FACE)){
//
//        }
//        else if(fontType.equals(AppConstants.MONTSERRAT_LIGHT_TYPE_FACE)){
//
//        }
    }


}

