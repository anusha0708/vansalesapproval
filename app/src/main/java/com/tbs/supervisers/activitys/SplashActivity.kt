package com.tbs.supervisers.activitys


import android.content.Intent
import android.os.Handler
import androidx.drawerlayout.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.supervisers.R
import com.tbs.supervisers.utils.PreferenceUtils


class SplashActivity : BaseActivity() {

    lateinit var llSplash: RelativeLayout
    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.dashboard, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

            Handler().postDelayed({

                if (preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "").isEmpty()) {


                    intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }else{
                    intent = Intent(this@SplashActivity, RequestsListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }

//            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            startActivity(intent)
//            finish()
        }, 2000)

    }

    override fun initializeControls() {
        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
    }

    public override fun onResume() {
        super.onResume()


    }



}
