package com.tbs.supervisers.database;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.tbs.supervisers.model.ConfigurationDo;
import com.tbs.supervisers.model.OrderRequestsDO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Kishore on 12/12/2018.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */

public class StorageManager {
    private static final String TAG = "StorageManager";

    private static TBSDatabaseHelper sDatabaseHelper;
    private static StorageManager sStorageManager;

    private StorageManager(Context context) {
        sDatabaseHelper = new TBSDatabaseHelper(context);
    }

    public static synchronized StorageManager getInstance(Context context) {
        if (sStorageManager == null) {
            sStorageManager = new StorageManager(context);
        }
        return sStorageManager;
    }

    //

    /**
     * Save Stock in Database
     */

    public void saveConfigurationDetails(Activity activity, ConfigurationDo customerDo) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ConfigurationDetails.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(customerDo);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveConfigurationDetails() : " + e.getMessage());
        }

    }

    public ConfigurationDo getConfigurationDetails(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ConfigurationDetails.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ConfigurationDo configurationDo = (ConfigurationDo) is.readObject();
                is.close();
                fis.close();
                return configurationDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getConfigurationDetails() : " + e.getMessage());
            return new ConfigurationDo();
        }
        return new ConfigurationDo();
    }

    public void saveRequestsList(Activity activity, ArrayList<OrderRequestsDO> customerDos) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SpotSalesCustomerList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(customerDos);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveSpotSalesCustomerList() : " + e.getMessage());
        }

    }

    public void deleteRequestsList(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SpotSalesCustomerList.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteSpotSalesCustomerList() : " + e.getMessage());
        }

    }

    public ArrayList<OrderRequestsDO> getRequestsList(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "SpotSalesCustomerList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<OrderRequestsDO> customerDos = (ArrayList<OrderRequestsDO>) is.readObject();
                is.close();
                fis.close();
                return customerDos;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getSpotSalesCustomerList() : " + e.getMessage());
            return new ArrayList<OrderRequestsDO>();
        }
        return new ArrayList<OrderRequestsDO>();
    }

}
