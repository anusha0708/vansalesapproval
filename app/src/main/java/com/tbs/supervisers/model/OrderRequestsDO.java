package com.tbs.supervisers.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 03-Nov-17.
 */

public class OrderRequestsDO implements Serializable {

    public int signatureLevel = 0;
    public int documentType = 0;
    public int sequenceID = 0;
    public String requestID = "";
    public int lineNumber = 0;
    public String loanDeliveryNumber = "";
    public String rescheduleDate = "";
    public String userID = "";
    public String userName = "";
    public String phoneNumber = "";
    public String deliveryNumber = "";
    public String date = "";
    public String time = "";
    public String documentDescription = "";
    public String product = "";
    public String productDescription = "";
    public double actualQuantity = 0.0;
    public double updatedQuantity = 0.0;
    public double actualPrice = 0.0;
    public double updatedPrice = 0.0;
    public String reasons = "";
    public String comments = "";
    public String unit = "";
    public String customerName = "";
    public String address = "";
    public String cityEmirates = "";
    public String signedUser1 = "";
    public String signedUser2 = "";
    public String requestedDate = "";
    public String requestedTime = "";
    public String requestedType = "";
    public String customer = "";
    public String status = "";
    public String docStatus = "";


}
