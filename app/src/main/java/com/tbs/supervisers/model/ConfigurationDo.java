package com.tbs.supervisers.model;


import java.io.Serializable;

public class ConfigurationDo implements Serializable {

    public String ipAddress = "";
    public String portNumber = "";
    public String poolAlias = "";
    public String userName = "";
    public String password = "";

}
